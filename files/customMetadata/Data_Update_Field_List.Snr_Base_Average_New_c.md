<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Base_Average_New__c</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Boolean__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Current_Field_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>New_Field_Name__c</field>
        <value xsi:type="xsd:string">Base_Average_New__c</value>
    </values>
    <values>
        <field>Object_Type__c</field>
        <value xsi:type="xsd:string">Survey_Space_Type__c</value>
    </values>
    <values>
        <field>Sector__c</field>
        <value xsi:type="xsd:string">Snr</value>
    </values>
</CustomMetadata>
