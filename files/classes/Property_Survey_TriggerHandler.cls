public class Property_Survey_TriggerHandler {
    
    public static void HandleAfterUpdate(Map<Id,Property_Survey__c> NewMap,Map<Id,Property_Survey__c> OldMap){
        //  List<Queue__c> QueueList=new List<Queue__c>();
        Map<Id,Queue__c> QueueMap=new Map<Id,Queue__c>();
        Set<Id> SubmittedIdSet=new Set<Id>();
        List<Survey_Metric__c> surveyMetricList = new List<Survey_Metric__c> ();
        List<String> IdList=new List<String>();
        String PropertyId;
        List<Ops_pool_data__c> PoolDataList=new List<OPS_POOL_DATA__C>();
        Boolean SurveyMapChanged=false;
        Set<Id> GenerateJSONSet=new Set<Id>();
        
        set<String> NewAssignmentStatus=new set<String>{'In Team Lead Review','In QA Review','Ready for Publish','On Hold','Complete'};
            //List<Property_Survey__c> listPropSurvey = new List<Property_Survey__c>();
            for(Id NewId: NewMap.keyset()){
                IdList.add(NewId);
            }
        Map<Id,Property_Survey__c> SurveyMap=new Map<Id,Property_Survey__c>([Select Id,INCOMING_SURVEY_DATA__C,Property_Id__c,status__c,Survey_Sector__r.Name from property_Survey__c where Id=:IdList]);
        PopulateSurveyDataClass2 PopulateSurveyData=new PopulateSurveyDataClass2();
        for(Id NewId: NewMap.keyset()){
            if(OldMap.get(NewId).status__c!='Ready for Publish' && NewMap.get(Newid).status__c=='Ready for Publish'){
                GenerateJSONSet.add(Newid);
            }
            if(OldMap.get(NewId).Survey_Data_Needed__c && (OldMap.get(NewId).Survey_Data_Needed__c!=NewMap.get(NewId).Survey_Data_Needed__c) && (OldMap.get(NewId).Incoming_Survey_Data__c != NewMap.get(NewId).Incoming_Survey_Data__c && NewMap.get(NewId).Incoming_Survey_Data__c !=null)){
                System.Debug('<<<<ConditionTrue>>>>');
                PropertyId=NewMap.get(NewId).Property_Id__c;
                try{
                    //  Anantha 22/08 Made this a batch Solution 
                    //  HandleSurveyDataUpdate(SurveyMap,PopulateSurveyData,QueueMap,NewId,NewMap,OldMap);    
                    SurveyMap.get(NewId).put('Pending_JSON__c',true);
                    SurveyMapChanged=true;  
                }catch(exception e){
                    e.setMessage('Property Id=>'+PropertyId+' '+e.getMessage());
                    throw e;
                }
            }
            
            //    if(NewMap.get(Newid).call_status__c!=OldMap.get(Newid).call_status__c){
            if(((NewMap.get(Newid).status__c!=OldMap.get(Newid).status__c && NewMap.get(Newid).status__c=='In Team Lead Review') || (NewMap.get(Newid).status__c!=OldMap.get(Newid).status__c && NewMap.get(Newid).status__c=='In QA Review' && OldMap.get(Newid).status__c != 'In Team Lead Review') || (NewMap.get(Newid).status__c!=OldMap.get(Newid).status__c && NewMap.get(Newid).status__c=='Ready for Publish' && OldMap.get(Newid).status__c != 'In Team Lead Review' && OldMap.get(Newid).status__c != 'In QA Review')) && NewMap.get(Newid).call_status__c!=null){
                System.Debug('<<<<ENTERRRRRRRR>>>>'+SurveyMap);
                ProcessCallStatusChange(NewMap,OldMap,Newid,SurveyMap,PoolDataList);
                SurveyMapChanged=true;
            }
            if((NewMap.get(Newid).status__c!=OldMap.get(Newid).status__c && NewMap.get(Newid).status__c=='In Team Lead Review') || (NewMap.get(Newid).status__c!=OldMap.get(Newid).status__c && NewMap.get(Newid).status__c=='In QA Review' && OldMap.get(Newid).status__c != 'In Team Lead Review') || (NewMap.get(Newid).status__c!=OldMap.get(Newid).status__c && NewMap.get(Newid).status__c=='Ready for Publish' && OldMap.get(Newid).status__c != 'In Team Lead Review' && OldMap.get(Newid).status__c != 'In QA Review')){
                SubmittedIdSet.add(Newid);
            }
            /* if(NewMap.get(NewId).Survey_Data_Can_Be_Purged__c && (OldMap.get(NewId).Survey_Data_Can_Be_Purged__c!=NewMap.get(NewId).Survey_Data_Can_Be_Purged__c) ){
//createMetric(SurveyMap,NewId,NewMap,surveyMetricList);
}*/
            
        }
        if(QueueMap.size()>0){
            UpdatePropertyOwner(SurveyMap,PopulateSurveyData.PropertyIdtoOwnerMap);
            //Anantha 21/07/2017 Added for the University Served
            UpdateUniversityServed(SurveyMap,PopulateSurveyData.PropertyIdToUniversityServedMap);
            PopulateSurveyData.UpdateSpaceMap();
            PopulateSurveyData.updateAmenitySpaceMap();
            PopulateSurveyData.UpdateTenantSpaceMap();
            PopulateSurveyData.UpdateContactSpaceMap();
            //  PopulateSurveyData.UpdateSurveyCareTypeSpaceMap();
            
            //   insert PopulateSurveyData.samMap.values();
            upsert PopulateSurveyData.samMap.values() AmenitiesExtId__c;
            //   insert PopulateSurveyData.sfAptSurveyPlanMap.values();
            upsert PopulateSurveyData.sfAptSurveyPlanMap.values() SurveySpaceExtId__c;
            //   insert PopulateSurveyData.scMap.values();
            upsert PopulateSurveyData.scMap.values() SurveyContactExtId__c;
            //   system.debug('<<<<PopulateSurveyData.scMap.values()>>>>' +PopulateSurveyData.scMap.values());
            //   insert PopulateSurveyData.retTenantsMap.Values();
            upsert PopulateSurveyData.retTenantsMap.Values() TenantsExtId__c;
            //Anantha 17/06 commented as no longer needed
            //  insert PopulateSurveyData.snrSurveyPlanMap.Values();
            update SurveyMap.values();// this is to update the Property_Survey__C object with JSON data PRASOON 6/23/2017
            //    System.Debug('<<<<ENterSurveyMapfind>>>>'+SurveyMap.values());
            //update QueueList;    
            update QueueMap.values();
        }
        if(surveyMetricList.size()>0){
            insert surveyMetricList;
        }
        
        if(SurveyMapChanged){
            update SurveyMap.values(); 
            //      System.Debug('<<<<ENterSurveyMapChanged>>>>'+SurveyMap.values());
            //       system.debug('Here6=>'+PoolDataList);
            if(PoolDataList.size()>0){
                update PoolDataList;
            }
        } 
        
        //Logic to update the queue status whenever the prop survey status is changed
        
        UpdateQueueStatus(NewMap,OldMap);
        if(SubmittedIdSet.size()>0){
            createMetricRecords.createMetric(SubmittedIdSet);    
        }
        
        // Anantha 09/05/2018 Added to ensure that the Select Code is reflecting in the outbound JSON.
        List<Property_Survey__c> GenerateJSONPSList=new List<Property_Survey__c>();
        Boolean GenerateJSON=false;
        For(Id PSId:GenerateJSONSet){
            
            if(!NewMap.get(PSId).Survey_Data_Can_Be_Purged__c){	
                OutboundSurveyData OutboundSurveyData1=new OutboundSurveyData(PSId);
                Property_Survey__c PS=new Property_Survey__c();
                PS.Id=PSId;
                PS.Outgoing_Survey_Data__c=OutboundSurveyData1.prepareJSON();
                PS.Survey_Data_Ready_for_Foundation__c=true;
                if(NewMap.get(PSId).Outgoing_Survey_Data__c!=PS.Outgoing_Survey_Data__c){
                    GenerateJSONPSList.add(PS);    
                    GenerateJSON=true;
                }
            }
            
            
        }
        
        if(GenerateJSON){
            update GenerateJSONPSList;
        }
        //End of Anantha 09/05/2018
        
        //update SurveyMap.values();
        //  AssignQueue();
    }
    
    //Anantha 20/06 added to update the PropertyOwner.
    public static void UpdatePropertyOwner(Map<Id,Property_Survey__c> SurveyMap,Map<String,String> PropertyIdtoOwnerMap){
        if(PropertyIdtoOwnerMap.size()>0){
            Map<String,String> CompanyNameToIdMap=new Map<String,String>();
            List<Company__c> CompanyList=[Select Id,Name from Company__c where Name=:PropertyIdtoOwnerMap.values()];
            for(Company__c Comp:CompanyList){
                CompanyNameToIdMap.put(Comp.Name, Comp.Id);
            }
            for(String PropId:PropertyIdtoOwnerMap.keySet()){
                if(CompanyNameToIdMap.containsKey(PropertyIdtoOwnerMap.get(PropId))){
                    SurveyMap.get(PropId).PropertyOwnerNew__c=CompanyNameToIdMap.get(PropertyIdtoOwnerMap.get(PropId));
                    SurveyMap.get(PropId).PropertyOwnerCurrent__c=CompanyNameToIdMap.get(PropertyIdtoOwnerMap.get(PropId));   
                }else{
                    SurveyMap.get(PropId).PropertyOwnerNew__c=null;
                    SurveyMap.get(PropId).PropertyOwnerCurrent__c=null; 
                }
            }            
        }
    }
    
    //Anantha 21/07/2017
    public static void UpdateUniversityServed(Map<Id,Property_Survey__c> SurveyMap,Map<String,String> PropertyIdToUniversityServedMap){
        //   System.Debug('<<<<ENter>>>>'+SurveyMap);
        if(PropertyIdToUniversityServedMap!=null && PropertyIdToUniversityServedMap.size()>0){
            Map<String,String> UnivNameToIdMap=new Map<String,String>();
            for(Survey_MSA__c MSA:[Select Id,MSA_Abbreviation__c from Survey_MSA__c where MSA_Abbreviation__c=:PropertyIdToUniversityServedMap.values()]){
                UnivNameToIdMap.put(MSA.MSA_Abbreviation__c, MSA.Id);
            }
            for(String PropId:PropertyIdToUniversityServedMap.keySet()){
                if(UnivNameToIdMap.containskey(PropertyIdToUniversityServedMap.get(PropId))){
                    //      SurveyMap.get(PropId).University_Served_Current__c=UnivNameToIdMap.get(PropertyIdToUniversityServedMap.get(PropId));
                    SurveyMap.get(PropId).University_Served_New__c=UnivNameToIdMap.get(PropertyIdToUniversityServedMap.get(PropId));    
                }else{
                    //    SurveyMap.get(PropId).University_Served_Current__c=null;
                    SurveyMap.get(PropId).University_Served_New__c=null;
                }
            }
        }
        // System.Debug('<<<<ENterLast>>>>'+SurveyMap);
    }
    
    public static void UpdateQueueStatus(Map<Id,Property_Survey__c> NewMap,Map<Id,Property_Survey__c> OldMap){
        set<String> QueueIdSet=new set<String>();
        Map<String,Integer> PropSurveyStatusRankMap=new Map<String,Integer>{'In Progress'=>2,'In Team Lead Review'=>4,'In QA Review'=>4,
            'Under Review'=>4,'Ready for Publish'=>7,'Returned to User'=>2,
            'Pending Creation'=>0,'Pending Assignment'=>1,'On Hold'=>3,'Failed to Load to Foundation'=>5};
                Map<String,String> QueueStatusMap=new Map<String,String>{'In Team Lead Review'=>'Under Review','In QA Review'=>'Under Review',
                    'In Progress'=>'In Progress','Ready for Publish'=>'Ready for Publish','Assigned'=>'Assigned','Pending Creation'=>'Pending Creation','Pending Assignment'=>'Pending Assignment','On Hold'=>'On Hold','Failed to Load to Foundation'=>'Failed to Load to Foundation','Returned to User'=>'Returned to User'};
                        
                        for(Property_Survey__c PropSurvey:NewMap.values()){
                            if((NewMap.get(PropSurvey.id).Status__c!=OldMap.get(PropSurvey.id).Status__c)){
                                QueueIdSet.add(PropSurvey.Queue_Record__c);    
                            }
                        }
        if(QueueIdSet.size()>0){
            List<Queue__c> QueueList=[Select id,Status__c,(select Id,Status__c, queue_record__c from property_surveys__r) from queue__c where Id =:QueueIdSet];
            Map<String,Queue__c> QueueIdtoQueueMap=new Map<String,Queue__c>();
            for(queue__c QueueRec:QueueList){
                for(Property_Survey__c PropSurvey:QueueRec.Property_Surveys__r){
                    if(QueueIdtoQueueMap.containsKey(QueueRec.id)){
                        if(PropSurveyStatusRankMap.get(PropSurvey.Status__c)<PropSurveyStatusRankMap.get(QueueIdtoQueueMap.get(QueueRec.id).status__c)){
                            QueueIdtoQueueMap.get(QueueRec.id).status__c=QueueStatusMap.get(PropSurvey.Status__c);
                        }
                    }else{
                        Queue__c updateQueueRec;
                        updateQueueRec=new Queue__c(Id=QueueRec.Id,Status__c=QueueStatusMap.get(PropSurvey.Status__c));
                        QueueIdtoQueueMap.put(QueueRec.Id, updateQueueRec);    
                        
                    }
                    
                } 
                
            }
            update QueueIdtoQueueMap.values();
        }
        
    }
    
    
    public static void HandleBeforeUpdate(Map<Id,Property_Survey__c> NewMap,Map<Id,Property_Survey__c> OldMap){
        List<Property_Survey__c> listPropSurvey = new List<Property_Survey__c>();
        List<String> IdList=new List<String>();
        set<id> SubmittedIdSet=new set<Id>();
        for(Id NewId: NewMap.keyset()){
            IdList.add(NewId);
        }
        Map<Id,Property_Survey__c> SurveyMap=new Map<Id,Property_Survey__c>([Select Id,Survey_Data_Ready_for_Foundation__c,INCOMING_SURVEY_DATA__C,Property_Id__c,status__c,Survey_Sector__r.Name from property_Survey__c where Id=:IdList]);
        
        for(Id NewId: NewMap.keyset()){
            if(NewMap.get(NewId).Foundation_Failed_to_Load_Reason__c!=null && NewMap.get(NewId).Foundation_Failed_to_Load_Reason__c!=OldMap.get(NewId).Foundation_Failed_to_Load_Reason__c){
                NewMap.get(NewId).status__c='Failed to Load to Foundation';
            }
            if(OldMap.get(NewId).Status__c!=NewMap.get(NewId).Status__c && NewMap.get(NewId).Status__c == 'Ready for Publish'){
                // OutboundSurveyData.prepareJSON  
                set<String> OnHoldCallStatus=new set<String>{'\'Reached Voicemail - Left Message\''};
                    
                    if(NewMap.get(NewId).Select_Code__c=='Z' || NewMap.get(NewId).Select_Code__c=='E'){
                        //   NewMap.get(NewId).Survey_Data_Can_Be_Purged__c=true;    
                    }else{
                        if(OnHoldCallStatus.contains('\''+NewMap.get(Newid).call_status__c+'\'')){
                            NewMap.get(NewId).Status__c='On Hold'; 
                        }
                        
                        //Anantha 09/05/2018 Commented and moved the JSON generation to AfterUpdate.     
                        //       OutboundSurveyData OutboundSurveyData1=new OutboundSurveyData(NewId);
                        //        NewMap.get(NewId).Outgoing_Survey_Data__c=OutboundSurveyData1.prepareJSON();
                        //        checkFlag (SurveyMap,NewId,NewMap,listPropSurvey);
                    }
            }
            
            if(OldMap.get(NewId).Survey_Data_Needed__c!=NewMap.get(NewId).Survey_Data_Needed__c && NewMap.get(NewId).Survey_Data_Needed__c){
                if(NewMap.get(NewId).Select_Code__c=='Z' || NewMap.get(NewId).Select_Code__c=='E'){
                    NewMap.get(NewId).Survey_Data_Can_Be_Purged__c=true; 
                    NewMap.get(NewId).Survey_Data_Needed__c=false;
                    NewMap.get(NewId).Status__c = 'Ready for Publish';
                    SubmittedIdSet.add(NewId);
                }    
            }
            //Anantha 28/09: RSI-595 change
            if(OldMap.get(NewId).Survey_Data_Can_Be_Purged__c!=NewMap.get(NewId).Survey_Data_Can_Be_Purged__c && NewMap.get(NewId).Survey_Data_Can_Be_Purged__c){
                if(NewMap.get(NewId).Status__c=='Ready for Publish')
                    SubmittedIdSet.add(NewId);
            }
        }
        
        if(SubmittedIdSet.size()>0){
            createMetricRecords.createMetric(SubmittedIdSet);    
        }
    }
    
    /*
// Anantha 22/08 Converted this to a batch solution
public static void HandleSurveyDataUpdate( Map<Id,Property_Survey__c> SurveyMap,PopulateSurveyDataClass2 PopulateSurveyData,Map<Id,Queue__c> QueueMap,String NewId,Map<Id,Property_Survey__c> NewMap,Map<Id,Property_Survey__c> OldMap){
system.debug('NewMap=>'+NewMap);
system.debug('OldMap=>'+OldMap);
system.debug('NewId=>'+NewId);
if(NewMap.get(NewId).queue_record__c !=null){
SurveyMap.get(NewId).put('Status__c','Pending Assignment');
PopulateSurveyData.updateSurvey(SurveyMap.get(NewId));
Queue__C QueueRecord=new Queue__c(Id=NewMap.get(NewId).queue_record__c,status__c='Pending Assignment');
QueueMap.put(NewMap.get(NewId).queue_record__c, QueueRecord);
//QueueList.add(QueueRecord);    
}
}
*/
    
    //to check the outgoing flag true
    public static void checkFlag(Map<Id,Property_Survey__c> SurveyMap,String NewId,Map<Id,Property_Survey__c> NewMap,List<Property_Survey__c> listPropSurvey){
        if(NewMap.get(NewId).Id !=null){
            NewMap.get(NewId).Survey_Data_Ready_for_Foundation__c = true;
        }
    }
    
    public static void ProcessCallStatusChange(Map<Id,Property_Survey__c> NewMap,Map<Id,Property_Survey__c> OldMap,Id NewId,Map<Id,Property_Survey__c> SurveyMap,List<Ops_pool_data__c> PoolDataList){
        
        set<String> OnHoldCallStatus=new set<String>{'\'Reached Voicemail - No Message\'','\'Told to Call Back\'',
            '\'Ring Out\'','\'Busy Signal\'','\'Contact Not Available\'','\'Contact Not in Office\'','\'Left On Hold\''};
                set<String> OnMonthHoldCallStatus=new set<String>{'\'Sent to Web - No Info\'','\'E-mail - No Info\'','\'Fax - No Info\'',
                    '\'Number Blocked\'','\'Receptionist Refused - No Info\''};
                        set<String> OnSelectCodeCallStatus=new set<String>{'\'Bad Number\'','\'Bad Contact\'','\'Proposed Type Change\'',
                            '\'Do Not Call - No Info\'','\'Contact Refused - No Info\''};
                                Map<String,String> CallStatusToSelectCodeMap=new Map<String,String>{'Bad Number'=>'Z','Bad Contact'=>'Z','Proposed Type Change'=>'T','Do Not Call - No Info'=>'K','Contact Refused - No Info'=>'R'};
                                    Map<String,String> SelectCodeToSelectCodeMap=new Map<String,String>{'Z'=>'IAZ','T'=>'IAT','K'=>'IAK','R'=>'IAR'};
                                        
                                        List<String> OnMonthHoldPropId=new List<String>();
        List<Id> PropIdList=new List<id>();
        Boolean isOnMonthHoldExist=false;
        Boolean isOnHoldExist=false;
        
        List<Property_Survey__c> UpdatePropSurveyList=new List<Property_Survey__c>();
        String PoolQueueId=[Select Id from Group where name='Pool Queue'].Id;
        if(OnHoldCallStatus.contains('\''+NewMap.get(Newid).call_status__c+'\'')){
            isOnHoldExist=true;
            //        This check exist on BeforeUpdate, hence commented
            //        SurveyMap.get(Newid).status__c='On Hold';
        }else if(OnMonthHoldCallStatus.contains('\''+NewMap.get(Newid).call_status__c+'\'')){
            //      system.debug('Here1');
            isOnMonthHoldExist=true;
            OnMonthHoldPropId.add(NewMap.get(Newid).Property_Id__c);
            //     SurveyMap.get(Newid).status__c='On Hold';
        }else if(OnSelectCodeCallStatus.contains('\''+NewMap.get(Newid).call_status__c+'\'')){
            if(CallStatusToSelectCodeMap.containsKey(NewMap.get(Newid).call_status__c)){
                if(NewMap.get(Newid).Select_Code__c=='IAA' || NewMap.get(Newid).Select_Code__c=='IAU'){
                    SurveyMap.get(Newid).select_code_new__c=SelectCodeToSelectCodeMap.get(CallStatusToSelectCodeMap.get(NewMap.get(Newid).call_status__c));
                }else{
                    SurveyMap.get(Newid).select_code_new__c=CallStatusToSelectCodeMap.get(NewMap.get(Newid).call_status__c);    
                }
                
            }
        }
        
        if(isOnHoldExist || isOnMonthHoldExist){
            //      system.debug('Here2');
            if(isOnMonthHoldExist){
                
                //       system.debug('Here4=>'+OnMonthHoldPropId);
                List<Ops_pool_data__c> PoolDataList1=[Select Id,Next_Queue_Date__c from ops_pool_data__c where property_id__c=:OnMonthHoldPropId];
                for(Ops_pool_data__c PoolData:PoolDataList1){
                    PoolData.Next_Queue_Date__c=Date.Today().addMonths(1);
                }
                PoolDataList.addAll(PoolDataList1);
                //        system.debug('Here5=>'+PoolDataList);
                //    update PoolDataList;
            }
            //     update QueueMap.values();
            //update UpdatePropSurveyList;
        }
    }
    
}