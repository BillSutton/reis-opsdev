public class REISTenantsTriggerHandlerClass {
    public static void HandleBeforeInsert(List<REIS_Tenant__c> NewList){
        
        populateUniqueString(NewList);
    }
    
    //populate 36characters long string
    public static void populateUniqueString(List<REIS_Tenant__c> NewList){
        for(REIS_Tenant__c REISTenant: NewList){
            if((REISTenant.Oid__c== '' || REISTenant.Oid__c== null)){
                Blob b = Crypto.GenerateAESKey(128);
                String h = EncodingUtil.ConvertTohex(b);
                String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
                system.debug(guid);
                REISTenant.OID__c=guid.touppercase();
                REISTenant.Salesforce_Created__c=true;
            }
            
        }
        
    }    

}