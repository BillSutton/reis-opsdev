public class InboundJSONProcessorClass {
    //Anantha Added this comment for version 3.5
    public static void ProcessJSON(List<sObject> scope){
        Map<Id,Property_Survey__C> SurveyMap=new Map<Id,Property_Survey__C>();
        Map<Id,Queue__c> QueueMap=new Map<Id,Queue__c>();
        for(sObject Scoperecord:scope){
            Property_Survey__C SurveyRecord=(Property_Survey__c)ScopeRecord;
            SurveyMap.put(SurveyRecord.Id, SurveyRecord);
        }//end of for scope
        PopulateSurveyDataClass2 PopulateSurveyData=new PopulateSurveyDataClass2();
        for(Id SurveyId:SurveyMap.keySet()){
            HandleSurveyDataUpdate(SurveyMap,PopulateSurveyData,QueueMap,SurveyId);
        }
        if(QueueMap.size()>0){
            UpdatePropertyOwner(SurveyMap,PopulateSurveyData.PropertyIdtoOwnerMap);
            //Anantha 21/07/2017 Added for the University Served
            UpdateUniversityServed(SurveyMap,PopulateSurveyData.PropertyIdToUniversityServedMap);
            PopulateSurveyData.UpdateSpaceMap();
            PopulateSurveyData.updateAmenitySpaceMap();
            PopulateSurveyData.UpdateTenantSpaceMap();
            PopulateSurveyData.UpdateContactSpaceMap();

            upsert PopulateSurveyData.samMap.values() AmenitiesExtId__c;
            upsert PopulateSurveyData.sfAptSurveyPlanMap.values() SurveySpaceExtId__c;
            upsert PopulateSurveyData.scMap.values() SurveyContactExtId__c;
            system.debug('<<<<PopulateSurveyData.scMap.values()>>>>' +PopulateSurveyData.scMap.values());
            upsert PopulateSurveyData.retTenantsMap.Values() TenantsExtId__c;
            update SurveyMap.values();// this is to update the Property_Survey__C object with JSON data PRASOON 6/23/2017
            update QueueMap.values();
        }
    }
    
    public static void HandleSurveyDataUpdate(Map<Id,Property_Survey__c> SurveyMap,PopulateSurveyDataClass2 PopulateSurveyData,Map<Id,Queue__c> QueueMap,String SurveyId){
            SurveyMap.get(SurveyId).put('Status__c','Pending Assignment');
            SurveyMap.get(SurveyId).put('Pending_JSON__c',false);
            PopulateSurveyData.updateSurvey(SurveyMap.get(SurveyId));
            Queue__C QueueRecord=new Queue__c(Id=SurveyMap.get(SurveyId).queue_record__c,status__c='Pending Assignment');
            QueueMap.put(SurveyMap.get(SurveyId).queue_record__c, QueueRecord);
            //QueueList.add(QueueRecord);    
    }
    
    public static void UpdatePropertyOwner(Map<Id,Property_Survey__c> SurveyMap,Map<String,String> PropertyIdtoOwnerMap){
        if(PropertyIdtoOwnerMap.size()>0){
            Map<String,String> CompanyNameToIdMap=new Map<String,String>();
            List<Company__c> CompanyList=[Select Id,Name from Company__c where Name=:PropertyIdtoOwnerMap.values()];
            for(Company__c Comp:CompanyList){
                CompanyNameToIdMap.put(Comp.Name, Comp.Id);
            }
            for(String PropId:PropertyIdtoOwnerMap.keySet()){
                if(CompanyNameToIdMap.containsKey(PropertyIdtoOwnerMap.get(PropId))){
                    SurveyMap.get(PropId).Property_Owner_New__c=CompanyNameToIdMap.get(PropertyIdtoOwnerMap.get(PropId));
                    SurveyMap.get(PropId).Property_Owner_Current__c=CompanyNameToIdMap.get(PropertyIdtoOwnerMap.get(PropId));   
                }else{
                    SurveyMap.get(PropId).Property_Owner_New__c=null;
                    SurveyMap.get(PropId).Property_Owner_Current__c=null; 
                }
            }            
        }
    }
    
    //Anantha 21/07/2017
    public static void UpdateUniversityServed(Map<Id,Property_Survey__c> SurveyMap,Map<String,String> PropertyIdToUniversityServedMap){
		System.debug('<<<<SurveyMap>>>>'+SurveyMap);
        if(PropertyIdToUniversityServedMap!=null && PropertyIdToUniversityServedMap.size()>0){
            Map<String,String> UnivNameToIdMap=new Map<String,String>();
            for(Survey_MSA__c MSA:[Select Id,MSA_Abbreviation__c from Survey_MSA__c where MSA_Abbreviation__c=:PropertyIdToUniversityServedMap.values()]){
                UnivNameToIdMap.put(MSA.MSA_Abbreviation__c, MSA.Id);
            }
            for(String PropId:PropertyIdToUniversityServedMap.keySet()){
                if(UnivNameToIdMap.containskey(PropertyIdToUniversityServedMap.get(PropId))){
                    SurveyMap.get(PropId).University_Served_Current__c=UnivNameToIdMap.get(PropertyIdToUniversityServedMap.get(PropId));
                    //SurveyMap.get(PropId).University_Served_New__c=UnivNameToIdMap.get(PropertyIdToUniversityServedMap.get(PropId));    
                }else{
                    SurveyMap.get(PropId).University_Served_Current__c=null;
                    //SurveyMap.get(PropId).University_Served_New__c=null;
                }
            }
        }
		//System.debug('<<<<SurveyMapEnd>>>>'+SurveyMap.get(PropId).University_Served_Current__c);
    }

}