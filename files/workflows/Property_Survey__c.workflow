<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>TT_Update_Ready_to_be_Purged_Flag</fullName>
        <field>Survey_Data_Can_Be_Purged__c</field>
        <literalValue>1</literalValue>
        <name>TT Update Ready to be Purged Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QC_Notes_with_Vac_Ref</fullName>
        <field>QC_Notes_New__c</field>
        <formula>IF( CONTAINS(QC_Notes_New__c, &quot;Vac Ref&quot;), QC_Notes_New__c, QC_Notes_New__c+ SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + TEXT(TODAY()) +&quot; - Vac Ref&quot;)</formula>
        <name>Update QC Notes with Vac Ref</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_DataUpdate_Flag</fullName>
        <field>Data_Update__c</field>
        <literalValue>1</literalValue>
        <name>Update the DataUpdate Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PropSurvey-Check if Proposed Type Change and IAG</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flag_Property_Survey_For_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QA_Update_Proposed_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Select_IAG_Type_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_the_DataUpdate_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This checks if the call status has been changed to proposed type change.</description>
        <formula>ISCHANGED(Call_Status__c)&amp;&amp;(ISPICKVAL( Call_Status__c , &apos;Proposed Type Change&apos;))&amp;&amp;((ISPICKVAL(Select_Code_New__c , &apos;IAG&apos;)) || (ISPICKVAL(Select_Code_New__c , &apos;IAZ&apos;)) || (ISPICKVAL(Select_Code_New__c , &apos;IAI&apos;)) || (ISPICKVAL(Select_Code_New__c , &apos;IAA&apos;)) || (ISPICKVAL(Select_Code_New__c , &apos;IAR&apos;)) || (ISPICKVAL(Select_Code_New__c , &apos;IAK&apos;)) || (ISPICKVAL(Select_Code_New__c , &apos;IAT&apos;)) || (ISPICKVAL(Select_Code_New__c , &apos;IAU&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey-Check if Proposed Type Change and Not IAG</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flag_Property_Survey_For_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QA_Update_Proposed_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Select_Type_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_the_DataUpdate_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This checks if the call status has been changed to proposed type change.</description>
        <formula>ISCHANGED(Call_Status__c)&amp;&amp;(ISPICKVAL(Call_Status__c , &apos;Proposed Type Change&apos;))&amp;&amp; !((ISPICKVAL(Select_Code_New__c , &apos;IAG&apos;)) ||  (ISPICKVAL(Select_Code_New__c , &apos;IAZ&apos;)) ||  (ISPICKVAL(Select_Code_New__c , &apos;IAI&apos;)) ||  (ISPICKVAL(Select_Code_New__c , &apos;IAA&apos;)) ||  (ISPICKVAL(Select_Code_New__c , &apos;IAR&apos;)) ||  (ISPICKVAL(Select_Code_New__c , &apos;IAK&apos;)) ||  (ISPICKVAL(Select_Code_New__c , &apos;IAT&apos;)) ||  (ISPICKVAL(Select_Code_New__c , &apos;IAU&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Survey - Vac Ref Check</fullName>
        <actions>
            <name>Update_QC_Notes_with_Vac_Ref</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Property_Survey__c.Vac_Ref__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Property_Survey__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Affordable,Apartments</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
