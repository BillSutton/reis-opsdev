<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Flag_Property_Survey_For_QA_Review</fullName>
        <field>Needs_QA_Review__c</field>
        <literalValue>1</literalValue>
        <name>Flag Property Survey For QA Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PropSurvey_Set_QC_Note_Renovation</fullName>
        <field>QC_Notes_New__c</field>
        <formula>QC_Notes_New__c + BR() +&quot;Renovation Date:&quot;+ TEXT(Month( Renovation_Date_New__c ))+&quot;/&quot;+ TEXT(DAY(Renovation_Date_New__c)) +&quot;/&quot;+TEXT(Year(Renovation_Date_New__c))</formula>
        <name>PropSurvey: Set QC Note for Renovation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QA_Update_Property_Type_Change</fullName>
        <field>QA_Reason_Notes__c</field>
        <formula>(QA_Reason_Notes__c + SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + &apos;(&apos; + TEXT(MONTH(TODAY()))+&apos;-&apos;+TEXT(DAY(TODAY())) + &apos;-&apos; + TEXT(YEAR(TODAY()))+&apos;)&apos;+&quot; Total Property Size changed to &quot; +   TEXT(Total_Property_Size_New__c)  )</formula>
        <name>QA Update Property Type Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SurveyDataCanBePurged_Changes_To_False</fullName>
        <field>Survey_Data_Can_Be_Purged__c</field>
        <literalValue>0</literalValue>
        <name>SurveyDataCanBePurged Changes To False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_In_Team_Lead_Review</fullName>
        <field>Status__c</field>
        <literalValue>In Team Lead Review</literalValue>
        <name>Update Status to In Team Lead Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Property Survey - QA Check - Check for Size Change</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QA_Update_Property_Type_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Check if total property size has been changed &gt;=20000 and Trigger a QA review</description>
        <formula>Total_Property_Size_New__c !=  Total_Property_Size_Current__c &amp;&amp; !ISNULL(Total_Property_Size_New__c) &amp;&amp; (ABS(Total_Property_Size_New__c-Total_Property_Size_Current__c)&gt;=20000 )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
