Global class InboundJSONProcessingBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.STATEFUL{
    String Sector;
    public InboundJSONProcessingBatch(String Sector){
        this.Sector=Sector;
    }
    global Database.querylocator start(Database.BatchableContext BC){
        String ProcessQuery='Select Id,Queue_Record__c,INCOMING_SURVEY_DATA__C,Property_Id__c,status__c,Survey_Sector__r.Name from property_Survey__c where Pending_JSON__c = true'+' AND Survey_Sector__r.Name=\''+Sector+'\''; 
        return Database.getQueryLocator(ProcessQuery);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
         InboundJSONProcessorClass.ProcessJSON(scope);
    }
    global void finish(Database.BatchableContext BC){
        
    }

}