public class SurveyContactTriggerHandlerClass {
   
    public static void HandleAfterInsert(Map<Id,Survey_Contact__c> NewMap){
    
       populateUniqueString(NewMap);
   
    }
    
    //populate 36characters long string
    public static void populateUniqueString(Map<Id,Survey_Contact__c> NewMap){
    List<Survey_Contact__c> SurveyContactList=new List<Survey_Contact__c>();
    for(String SurveyContactId: NewMap.keyset()){
        if(NewMap.get(SurveyContactId).Id !=null && (NewMap.get(SurveyContactId).Contact_Oid__c== '' || NewMap.get(SurveyContactId).Contact_Oid__c== null)){
            Blob b = Crypto.GenerateAESKey(128);
                String h = EncodingUtil.ConvertTohex(b);
                String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
                system.debug(guid);
            Survey_Contact__c SurveyContact=new Survey_Contact__c(Id=NewMap.get(SurveyContactId).Id ,Contact_Oid__c= guid.touppercase());
            SurveyContactList.add(SurveyContact);
        }
       
    }
    update SurveyContactList;
 }
    
  
 }